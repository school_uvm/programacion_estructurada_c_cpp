# Programación estructurada C/C++ (Semestre 4 UVM)

Recopilación de los ejercicios hechos en la clase de logica y programación estructurada del **cuatrimestre 4** (UVM).

Esto con fin de guardar las actividades sin perderlas en caso de borrar la carpeta contenedora. No usar este proyecto con fines de plagio ya que facilmente podría ser atrapado usando trampas para la escuela (le recomiendo usarlo con fines de aprendizaje solamente).

## Actividades

- Actividad 1 (Esta actividad no se hace nada)
- Actividad 2
- [Actividad 3](./Actividad%203/)
- [Actividad 4](./Poyecto%20Integrador/)
- [Actividad 5](./Actividad%205/)
- [Actividad 6](#)
- [Actividad 7](#)
- [Actividad 8](#)


### Notas

Todas las actividades listadas fueron compiladas y testeadas con:

- Compiladores compatibles:
    - GNU GCC 8.1.0 x86_64-mingw (C++ 17)
    - Visual studio 2022 (amd64)
    - Visual studio 2022 (x86 o x86_amd64)
- CMake (v3.22 o superior)