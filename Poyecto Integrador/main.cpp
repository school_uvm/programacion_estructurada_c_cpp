#include "base/deps.h"

void applicationExit(const content::menu::MenuItem &item) {
	// Only close the application
	actions::database::releaseDatabase();
	std::exit(0);
}


int main() {
	// Initialize databas
	actions::database::initializeDatabase();

	// Generate menu
	auto options = utils::print::basicOptions("University Menu");
	options.line_sep = "*";
	options.border = "*";

	auto mainMenu = std::make_shared<content::menu::Menu>("", "", utils::print::makeSection(options));

	// Insert all actions
	mainMenu->addItem(std::make_shared<content::menu::MenuItem>("Register new student",
	                                                            "Generate new student",
	                                                            actions::database::insertStudentAction));
	mainMenu->addItem(std::make_shared<content::menu::MenuItem>("Remove student",
	                                                            "Remove existing student",
	                                                            actions::database::removeStudentAction));
	mainMenu->addItem(std::make_shared<content::menu::MenuItem>("Search student",
	                                                            "Search an specific student",
	                                                            actions::database::searchStudentAction));
	mainMenu->addItem(std::make_shared<content::menu::MenuItem>("List all students",
	                                                            "Show all students",
	                                                            actions::database::showAllStudentsAction));
	mainMenu->addItem(std::make_shared<content::menu::MenuItem>("Exit", "Quit application", applicationExit));

	// Display the main menu
	mainMenu->showMenu(std::cout);
	return 0;
}