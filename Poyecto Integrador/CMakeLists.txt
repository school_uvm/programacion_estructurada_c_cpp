cmake_minimum_required(VERSION 3.20)
set(CMAKE_CXX_STANDARD 17)

set(UTILS_CONTENT_FILES
        utils/input.cpp
        utils/print.cpp
        utils/casting.h
        utils/errors.h
        utils/input.h
        utils/utils.h
        utils/print.h
        utils/ptr.h)

set(BASE_CONTENT_FILES
        base/deps.h
        base/raw_deps.h
        base/file/files.cpp
        base/file/files.h
        base/content/ContentManager.h
        base/content/strings.h
        base/content/strings.cpp
        base/content/content.h
        base/content/constants.h)

set(BASE_MENU_FILES
        base/menu/Menu.h
        base/menu/Menu.cpp
        base/menu/MenuItem.cpp)

set(BASE_ACTIONS_FILES
        base/actions/actions.h
        base/actions/Student.cpp
        base/actions/Student.h
        base/actions/dbActions.cpp
        base/actions/dbActions.h)

project(ProyectoIntegrador)
add_executable(ProyectoIntegrador
        main.cpp
        ${UTILS_CONTENT_FILES}
        ${BASE_CONTENT_FILES}
        ${BASE_MENU_FILES}
        ${BASE_ACTIONS_FILES})