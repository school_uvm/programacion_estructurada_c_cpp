#include <iostream>
#include <memory>

#ifndef PROYECTOINTEGRADOR_PTR_H
#define PROYECTOINTEGRADOR_PTR_H

namespace utils {

	/**
	 * Function shortcut
	 *
	 * @tparam T Target object type
	 * @tparam TArgs Type arguments
	 * @param args Arguments
	 * @return Returns a unique ptr instance
	 */
	template<typename T, typename... TArgs>
	[[nodiscard]] std::unique_ptr<T> unique(TArgs &&...args) {
		return std::make_unique<T>(args...);
	}

	/**
	 * Function shortcut
	 *
	 * @tparam T Target object type
	 * @tparam TArgs Type arguments
	 * @param args Arguments
	 * @return Returns a shared ptr instance
	 */
	template<typename T, typename... TArgs>
	[[nodiscard]] std::shared_ptr<T> shared(TArgs &&...args) {
		return std::make_shared<T>(args...);
	}

}// namespace utils

#endif//PROYECTOINTEGRADOR_PTR_H
