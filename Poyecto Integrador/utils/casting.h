#ifndef PROYECTOINTEGRADOR_CASTING_H
#define PROYECTOINTEGRADOR_CASTING_H

namespace utils::casting {

	/**
	 * Check if target object is a subclass or the same class of target reference
	 *
	 * @tparam TB Target object type
	 * @tparam TC Current object type
	 * @param obj The object to check
	 * @return Returns @c true if @c obj is a subclass or the same type of @c TB or @c false otherwise
	 */
	template<typename TB, typename TC>
	bool instanceOf(const TC *obj) {
		return dynamic_cast<const TB *>(obj) != nullptr;
	}

}

#endif//PROYECTOINTEGRADOR_CASTING_H
