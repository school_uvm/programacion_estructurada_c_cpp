#include "print.h"
#include <sstream>
#include <string>

utils::print::SectionOpt utils::print::basicOptions(std::string_view name, std::ostream &stream) {
	return utils::print::SectionOpt{.name = name, .stream = stream};
}

std::string utils::print::makeSection(const utils::print::SectionOpt &options) {
	// Generate result
	std::stringstream result;
	std::string separator = utils::print::strRepeat(options.border, options.line_width);
	std::string spaces_b = utils::print::strRepeat("\n", options.space_b);
	std::string spaces_a = utils::print::strRepeat("\n", options.space_a);

	// Generate content
	result << spaces_b << options.line_sep << separator << '\n'
	       << options.line_sep << " " << options.name << '\n'
	       << options.line_sep << separator << spaces_a;

	return result.str();
}

void utils::print::printSection(const utils::print::SectionOpt &options) {
	// Only make the section
	options.stream << makeSection(options);
}

std::string utils::print::printAsList(std::string_view content,
                                      long long int index,
                                      unsigned long long int spaces,
                                      unsigned long long int max) {
	// Generate result
	std::stringstream result;
	std::string maxStr = max == 0 ? std::to_string(index) : std::to_string(max);
	std::string indexStr = std::to_string(index);
	int diff = abs(static_cast<int>(maxStr.length()) - static_cast<int>(indexStr.length()));

	// Write the files
	std::string spacesStr = utils::print::strRepeat(" ", diff + spaces);
	result << spacesStr << std::to_string(index) << "." << spacesStr << content;

	return result.str();
}

std::string utils::print::strRepeat(std::string_view content, unsigned int length) {
	// Generate result
	std::string result{};

	// Verify if files is empty
	if (content.empty()) return result;

	// Iterate elements
	for (int i = 0; i < length; i++) { result += content; }

	return result;
}