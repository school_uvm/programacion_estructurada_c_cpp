#include "input.h"
#include <limits>

void utils::input::clearInput(std::istream &istream) {
	istream.clear();
	istream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void utils::input::wait(std::istream &istream, std::ostream &ostream) {
	// Show message
	ostream << '\n' << "Press enter to continue...";
	istream.get();
	clearInput(istream);
}