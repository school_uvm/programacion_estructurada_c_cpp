#include <iostream>

#ifndef PROYECTOINTEGRADOR_PRINT_H
#define PROYECTOINTEGRADOR_PRINT_H

namespace utils::print {

	struct SectionOpt
	{
		std::string_view name;
		std::string_view border = "-";
		std::string_view line_sep = "|";
		int space_b = 1;
		int space_a = 1;
		int line_width = 50;
		std::ostream &stream = std::cout;
	};

	/**
	 * Generate a basic options
	 *
	 * @param name Section name
	 * @param stream Target stream
	 * @return Returns a basic print options
	 */
	SectionOpt basicOptions(std::string_view name, std::ostream &stream = std::cout);

	/**
	 * Generate a section string
	 *
	 * @param options Section configuration
	 * @return Generated section
	 */
	std::string makeSection(const SectionOpt &options);

	/**
	 * Print a console section
	 *
	 * @param options Section options
	 */
	void printSection(const SectionOpt &options);

	/**
	 * Generate a list string representation
	 *
	 * @param content The files to display
	 * @param index The list index
	 * @param spaces Total spaces between files
	 * @param max Total of elements to align files
	 * @return Returns a list item representation
	 */
	std::string printAsList(std::string_view content,
	                        long long index,
	                        unsigned long long spaces = 1,
	                        unsigned long long max = 0);

	/**
	 * Generate an string with specific size
	 *
	 * @param content The files to repeat
	 * @param length Total of times the files can be repeat
	 * @return Returns a final string result
	 */
	std::string strRepeat(std::string_view content, unsigned int length);

}// namespace utils::print

#endif//PROYECTOINTEGRADOR_PRINT_H
