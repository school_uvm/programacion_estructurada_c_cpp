#include "input.h"
#include <iostream>

#ifndef PROYECTOINTEGRADOR_ERRORS_H
#define PROYECTOINTEGRADOR_ERRORS_H

namespace utils::errors {

	/**
	 * Launch any exception dynamically
	 *
	 * @tparam T Generic exception type
	 * @tparam TArgs Argument types
	 * @param args Exception arguments
	 */
	template<typename TE = std::runtime_error, typename... TArgs>
	void launch(TArgs &&...args) {
		static_assert(std::is_base_of_v<std::exception, TE>, "Is not valid exception type");
		throw TE(std::forward<TArgs>(args)...);
	}

}

#endif//PROYECTOINTEGRADOR_ERRORS_H
