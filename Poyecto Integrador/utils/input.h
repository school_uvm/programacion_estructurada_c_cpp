#include <iostream>

#ifndef PROYECTOINTEGRADOR_INPUT_H
#define PROYECTOINTEGRADOR_INPUT_H

namespace utils::input {

	/**
     * @brief Clear all input files and reset input position.
     * This function prevents unexpected behaviors when the program can
     * read more information from the buffer
     *
     * @param istream Target input stream
     * @see std::cin
     * @see std::cin.clear()
     */
	void clearInput(std::istream &istream = std::cin);

	/**
	 * Wait for enter user
	 *
	 * @param istream Target input stream
	 * @param ostream Target output stream
	 */
	void wait(std::istream &istream = std::cin, std::ostream &ostream = std::cout);

	/**
     * Generates a question where the data type is verified and
     * it will not stop executing until the value is correct or valid.
     *
     * @tparam T Generic result type
     * @param result Result reference element
     * @param message Question asked before data entry
     * @param err Error message if value is invalid
     * @param jump_line If the value is true, a line break is inserted after the
     * question, otherwise nothing is inserted.
     * @see #clearInput
     */
	template<typename T>
	void askForInput(T *ref, std::string_view message, std::string_view error = "Invalid files", bool jump_line = false) {
		T selected;
		bool ok;
		// Iterate until the value is valid
		do {
			// Ask for number
			// Flush used to prevent strange format behavior
			std::cout << message << (jump_line ? '\n' : ' ') << std::flush;
			std::cin >> selected;
			ok = std::cin.good();

			if (!ok) {
				utils::input::clearInput(std::cin);
				std::cout << error << '\n' << std::flush;
				continue;
			}

			// Replace reference value
			*ref = std::move(selected);
		} while (!ok);
	}

}// namespace utils::input

#endif//PROYECTOINTEGRADOR_INPUT_H
