#include "Student.h"
#include "../../utils/utils.h"
#include "../content/constants.h"
#include "../content/strings.h"
#include "../file/files.h"
#include <regex>
#include <sstream>
#include <string>

using content::CONTENT_SEPARATOR;
using content::EMAIL_REGEX_VALIDATOR;

/* -------------------------------------------------------------
 * Extra internal functions
 * ------------------------------------------------------------- */

void changeEl(const content::files::Line &line, content::files::Line *ref) { *ref = line; }

/* -------------------------------------------------------------
 * Constructors
 * ------------------------------------------------------------- */

actions::Student::Student() = default;

actions::Student::Student(std::string_view name,
                          std::string_view fLastName,
                          std::string_view mLastName,
                          std::string_view career,
                          std::string_view email,
                          std::string_view phone) {
	// Initialize all properties
	this->name = content::strings::trimC(std::string(name));
	this->fatherLastName = content::strings::trimC(std::string(fLastName));
	this->motherLastName = content::strings::trimC(std::string(mLastName));
	this->career = content::strings::trimC(std::string(career));
	setEmail(std::string(email));
	this->phone = content::strings::trimC(std::string(phone));
}


/* -------------------------------------------------------------
 * Getters
 * ------------------------------------------------------------- */

std::string_view actions::Student::getId() const {
	// Return the user id
	return id;
}

std::string_view actions::Student::getName() const {
	// Return the username
	return name;
}

std::string_view actions::Student::getFatherLastName() const {
	// Return the user father lastname
	return fatherLastName;
}

std::string_view actions::Student::getMotherLastName() const {
	// Return the user mother lastname
	return motherLastName;
}

std::string_view actions::Student::getCareer() const {
	// Return the user career
	return career;
}

std::string_view actions::Student::getEmail() const {
	// Return the user email
	return email;
}

std::string_view actions::Student::getPhone() const {
	// Return the user phone
	return phone;
}

/* -------------------------------------------------------------
 * Methods
 * ------------------------------------------------------------- */

std::string actions::Student::toFormatString() const {
	// Generate all files
	std::stringstream content;

	// Insert all information
	content << id << CONTENT_SEPARATOR << name << CONTENT_SEPARATOR << fatherLastName << CONTENT_SEPARATOR << motherLastName
	        << CONTENT_SEPARATOR << career << CONTENT_SEPARATOR << email << CONTENT_SEPARATOR << phone;

	return content.str();
}

std::string actions::Student::toString() const {// Generate all files
	std::stringstream content;

	// Insert all information
	content << id << " " << name << " " << fatherLastName << " " << motherLastName << " [" << career << "] (" << email << ") "
	        << phone;

	return content.str();
}

void actions::Student::askForContent(std::ostream &outStream, std::istream &inStream) {
	// Temporal variables
	std::regex emailRegex(content::EMAIL_REGEX_VALIDATOR);

	// For name
	outStream << "Insert the name: ";
	std::getline(inStream >> std::ws, name);

	// For name
	outStream << "Insert the father lastname: ";
	std::getline(inStream >> std::ws, fatherLastName);

	// For name
	outStream << "Insert the mother lastname: ";
	std::getline(inStream >> std::ws, motherLastName);

	// For name
	outStream << "Insert the career: ";
	std::getline(inStream >> std::ws, career);

	// For name
	do {
		try {
			std::string tmpEmail;
			outStream << "Insert the email: ";
			std::getline(inStream >> std::ws, tmpEmail);
			// Change email value
			setEmail(tmpEmail);
			break;
		} catch (std::exception &e) { outStream << "Error: " << e.what() << '\n'; }
	} while (true);

	// For name
	outStream << "Insert the phone: ";
	std::getline(inStream >> std::ws, phone);
}

void actions::Student::write(std::ostream &outStream, std::vector<std::shared_ptr<actions::Student>> &ref) {
	// At this moment is when the id must be assigned, but only if id is different of -1
	if (id == "-1") id = nextId(ref);
	// Write the content on the stream
	outStream << toFormatString() << '\n';
}

/* -------------------------------------------------------------
 * Internal methods
 * ------------------------------------------------------------- */

void actions::Student::setEmail(const std::string &tEmail) {
	std::string realEmail = content::strings::trimC(tEmail);
	// Temporal variables
	std::regex emailRegex(content::EMAIL_REGEX_VALIDATOR);
	// Verify if current email is valid
	if (!std::regex_match(realEmail, emailRegex)) {
		// Configure message error
		std::stringstream errStr;
		errStr << "The email \"" << realEmail << "\" "
		       << "does not have a valid email format";
		// Launch error
		utils::errors::launch(errStr.str());
	}
	// Change email content
	email = realEmail;
}

/* -------------------------------------------------------------
 * Static methods
 * ------------------------------------------------------------- */

std::shared_ptr<actions::Student> actions::Student::makeFromLine(const std::string &line) {
	std::string copy(line);// NOLINT(performance-unnecessary-copy-initialization)
	// Check if line is valid
	if (!validateLine(copy)) utils::errors::launch("Invalid line content.");

	std::vector<std::string> items = content::strings::split(copy, content::CONTENT_SEPARATOR);
	auto result = std::make_shared<actions::Student>(items[1], items[2], items[3], items[4], items[5], items[6]);
	result->id = items[0];

	return result;
}

std::string actions::Student::nextId(std::vector<std::shared_ptr<actions::Student>> &ref) {
	// Generate files menu
	std::stringstream content;
	content::files::Line currentId = 1;

	// Inspect database
	for (auto &item : ref) {
		char *buff;
		content::files::Line tmpId = std::strtol(item->id.c_str(), &buff, 10);

		if (tmpId == -1) continue;
		currentId = tmpId;
	}

	return std::to_string(++currentId);
}

bool actions::Student::validateLine(std::string &line) {
	// First check if line is not empty
	if (content::strings::isBlank(line)) return false;
	// Clean string
	content::strings::trim(line);
	std::vector<std::string> elements = content::strings::split(line, content::CONTENT_SEPARATOR);

	return elements.size() == actions::PREF_ITEM_ELEMENTS;
}
