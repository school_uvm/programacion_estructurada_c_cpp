#include "../menu/Menu.h"
#include <iostream>

#ifndef PROYECTOINTEGRADOR_DBACTIONS_H
#define PROYECTOINTEGRADOR_DBACTIONS_H

namespace actions::database {

	/* -------------------------------------------------------------
	 * Independent actions
	 * ------------------------------------------------------------- */

	/**
	 * Initialize database container
	 */
	void initializeDatabase();

	/**
	 * Write all content
	 */
	void writeAll();

	/**
	 * Release all database connections
	 */
	void releaseDatabase();

	/* -------------------------------------------------------------
	 * Menu actions
	 * ------------------------------------------------------------- */

	/**
	 * Show all students
	 *
	 * @param item The item selected
	 */
	void showAllStudentsAction(const content::menu::MenuItem &item);

	/**
	 * Search student action
	 *
	 * @param item The item selected
	 */
	void searchStudentAction(const content::menu::MenuItem &item);

	/**
	 * Insert student action
	 *
	 * @param item The item selected
	 */
	void insertStudentAction(const content::menu::MenuItem &item);

	/**
	 * Remove student action
	 *
	 * @param item The item selected
	 */
	void removeStudentAction(const content::menu::MenuItem &item);

}// namespace actions::database

#endif//PROYECTOINTEGRADOR_DBACTIONS_H
