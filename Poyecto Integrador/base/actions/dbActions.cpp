#include "dbActions.h"
#include "../../utils/utils.h"
#include "../content/strings.h"
#include "../file/files.h"
#include "../raw_deps.h"
#include "Student.h"

// Generate a global vector container
namespace actions::database {
	std::vector<std::shared_ptr<actions::Student>> _dbContainer;
	std::fstream _dbConnection;
}// namespace actions::database

/* -------------------------------------------------------------
 * Independent actions
 * ------------------------------------------------------------- */

bool sortDatabase(const std::shared_ptr<actions::Student> &one, const std::shared_ptr<actions::Student> &two) {
	char *buff;
	content::files::Line oneId = std::strtol(one->getId().data(), &buff, 10);
	content::files::Line twoId = std::strtol(two->getId().data(), &buff, 10);

	return (oneId < twoId);
}

void actions::database::initializeDatabase() {
	// Load stream
	_dbConnection = content::files::requireFile(content::files::STUDENTS_FILE_CONTENT);
	// Load all students
	content::files::readLines(_dbConnection, [](const content::files::Line &idx, const std::string &line) {
		try {
			auto student = actions::Student::makeFromLine(line);
			_dbContainer.push_back(student);
		} catch (...) {}
	});
	// Sort database elements
	std::sort(_dbContainer.begin(), _dbContainer.end(), sortDatabase);
}

void actions::database::writeAll() {
	// First truncate file
	content::files::truncateFile(content::files::STUDENTS_FILE_CONTENT, _dbConnection);
	// Write all content elements
	for (auto &student : _dbContainer) {
		// Insert element item by item
		student->write(_dbConnection, _dbContainer);
	}
	// Flush content
	_dbConnection.flush();
}

void actions::database::releaseDatabase() {
	// Write all content if exists
	writeAll();
	// Remove all elements from vector
	_dbContainer.clear();
	// Release open connection
	if (_dbConnection.is_open()) _dbConnection.close();
}

/* -------------------------------------------------------------
 * Menu actions
 * ------------------------------------------------------------- */

void actions::database::showAllStudentsAction(const content::menu::MenuItem &item) {
	// Print a section content
	auto options = utils::print::basicOptions("All students");
	options.space_a = 2;
	options.space_b = 2;

	utils::print::printSection(options);

	for (auto &student : _dbContainer) {
		// Display on the screen
		std::cout << student->toString() << '\n';
	}
}

void actions::database::searchStudentAction(const content::menu::MenuItem &item) {
	// Temporal variables
	std::string refId;
	auto options = utils::print::basicOptions("Search student");

	// Configure section
	options.space_a = 2;
	options.space_b = 2;

	utils::print::printSection(options);

	// Ask for student id
	utils::input::clearInput(std::cin);
	std::cout << "Insert the student id: ";
	std::getline(std::cin, refId);

	// Search element
	auto foundItem = std::find_if(_dbContainer.begin(), _dbContainer.end(), [&refId](std::shared_ptr<actions::Student> &item) {
		return item->getId() == content::strings::trimC(refId);
	});


	// Check if student was found
	if (foundItem == _dbContainer.end()) {
		std::stringstream errMsg;
		// Configure message
		errMsg << "The user id \"" << refId << "\" not found";
		utils::errors::launch(errMsg.str());
	}

	// Display element
	std::cout << foundItem->get()->toString() << '\n';
}

void actions::database::insertStudentAction(const content::menu::MenuItem &item) {
	// Temporal variables
	auto result = std::make_shared<actions::Student>();
	auto options = utils::print::basicOptions("Insert student");

	// Configure section
	options.space_a = 2;
	options.space_b = 2;

	utils::print::printSection(options);

	do {
		// Ask for student content
		result->askForContent(std::cout, std::cin);

		// Check if student already exists
		auto found = std::find_if(_dbContainer.begin(), _dbContainer.end(), [&result](std::shared_ptr<actions::Student> &item) {
			return item->getName() == result->getName() && item->getMotherLastName() == result->getMotherLastName() &&
			       item->getFatherLastName() == result->getFatherLastName();
		});

		if (found == _dbContainer.end()) break;
		std::cerr << "The user \"" << result->toString() << "\" already exists" << '\n';
	} while (true);

	// Insert all elements
	_dbContainer.push_back(result);
	writeAll();
}

void actions::database::removeStudentAction(const content::menu::MenuItem &item) {
	// Temporal variables
	std::string refId;
	auto options = utils::print::basicOptions("Search student");

	// Configure section
	options.space_a = 2;
	options.space_b = 2;

	utils::print::printSection(options);

	// Ask for student id
	utils::input::clearInput(std::cin);
	std::cout << "Insert the student id: ";
	std::getline(std::cin, refId);

	// Search element
	auto foundItem = std::find_if(_dbContainer.begin(), _dbContainer.end(), [&refId](std::shared_ptr<actions::Student> &item) {
		return item->getId() == content::strings::trimC(refId);
	});


	// Check if student was found
	if (foundItem == _dbContainer.end()) {
		std::stringstream errMsg;
		// Configure message
		errMsg << "The user id \"" << refId << "\" not found";
		utils::errors::launch(errMsg.str());
	}

	// Remove element from vector and write in database
	_dbContainer.erase(foundItem);
	writeAll();
}
