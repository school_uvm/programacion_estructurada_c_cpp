#include "../content/ContentManager.h"
#include <iostream>
#include <memory>
#include <vector>

#ifndef PROYECTOINTEGRADOR_STUDENT_H
#define PROYECTOINTEGRADOR_STUDENT_H

namespace actions {

	/**
	 * Number of elements that the content must have as a minimum
	 */
	const int PREF_ITEM_ELEMENTS = 7;

	/**
	 * Object used to represent student
	 */
	class Student : public content::FormatString, content::AskContent {
	private:

		/**
		 * Student id
		 */
		std::string id = std::to_string(-1);

		/**
		 * Student name
		 */
		std::string name;

		/**
		 * Student father lastname
		 */
		std::string fatherLastName;

		/**
		 * Student mother lastname
		 */
		std::string motherLastName;

		/**
		 * Student college career
		 */
		std::string career;

		/**
		 * Student email
		 */
		std::string email;

		/**
		 * Student phone
		 */
		std::string phone;

	public:

		/**
		 * Empty constructor
		 */
		Student();

		/**
		 * Constructor with all values
		 *
		 * @param name Student name
		 * @param fLastName Student father lastname
		 * @param mLastName Student mother lastname
		 * @param career Student career
		 * @param email Student email
		 * @param phone Student phone
		 */
		Student(std::string_view name,
		        std::string_view fLastName,
		        std::string_view mLastName,
		        std::string_view career,
		        std::string_view email,
		        std::string_view phone);

		/**
		 * Gets the current user id
		 *
		 * @return Returns the user id
		 */
		[[nodiscard]] std::string_view getId() const;

		/**
		 * Gets the current user name
		 *
		 * @return Returns the user name
		 */
		[[nodiscard]] std::string_view getName() const;

		/**
		 * Gets the current user father last name
		 *
		 * @return Returns the user father last name
		 */
		[[nodiscard]] std::string_view getFatherLastName() const;

		/**
		 * Gets the current user mother last name
		 *
		 * @return Returns the user mother last name
		 */
		[[nodiscard]] std::string_view getMotherLastName() const;

		/**
		 * Gets the current user career
		 *
		 * @return Returns the user career
		 */
		[[nodiscard]] std::string_view getCareer() const;

		/**
		 * Gets the current user email
		 *
		 * @return Returns the user email
		 */
		[[nodiscard]] std::string_view getEmail() const;

		/**
		 * Gets the current user phone
		 *
		 * @return Returns the user phone
		 */
		[[nodiscard]] std::string_view getPhone() const;

		/**
		 * Get current object string representation
		 *
		 * @return Object string representation
		 */
		[[nodiscard]] std::string toFormatString() const override;

		/**
		 * Pretty object string representation
		 *
		 * @return Pretty print representation
		 */
		[[nodiscard]] std::string toString() const override;

		/**
		 * Ask for initialization files
		 *
		 * @param outStream Output stream
		 * @param inStream Input stream
		 */
		void askForContent(std::ostream &outStream, std::istream &inStream) override;

		/**
		 * Write content on the target stream
		 *
		 * @param outStream The stream to write
		 */
		void write(std::ostream &outStream, std::vector<std::shared_ptr<actions::Student>> &ref);

		/**
		 * Make student from line
		 *
		 * @param line The line to inspect
		 * @return Returns a valid student element
		 */
		static std::shared_ptr<Student> makeFromLine(const std::string &line);

	protected:

		/**
		 * Change user tEmail
		 *
		 * @param tEmail The tEmail to insert
		 */
		void setEmail(const std::string &tEmail);

	private:

		/**
		 * Get next student id
		 *
		 * @param ref Database reference container
		 * @return Returns the next student id
		 */
		[[nodiscard]] static std::string nextId(std::vector<std::shared_ptr<actions::Student>> &ref);

		/**
		 * Validate selected line
		 *
		 * @param line The line to validate
		 * @return Return @c true if current line is valid or @c false otherwise
		 */
		[[nodiscard]] static bool validateLine(std::string &line);
	};

}// namespace actions

#endif//PROYECTOINTEGRADOR_STUDENT_H
