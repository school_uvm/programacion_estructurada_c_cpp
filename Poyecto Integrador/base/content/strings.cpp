#include "strings.h"
#include <algorithm>
#include <cctype>
#include <sstream>
#include <string>

void content::strings::lTrim(std::string &s) {
	// Remove all left blank files
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int c) { return !std::isspace(c); }));
}

void content::strings::rTrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int c) { return !std::isspace(c); }).base(), s.end());
}

void content::strings::trim(std::string &s) {
	lTrim(s);
	rTrim(s);
}

std::string content::strings::lTrimC(std::string s) {
	lTrim(s);
	return s;
}

std::string content::strings::rTrimC(std::string s) {
	rTrim(s);
	return s;
}

std::string content::strings::trimC(std::string s) {
	trim(s);
	return s;
}


bool content::strings::isBlank(std::string s) {
	// Clean the copy
	trim(s);
	// Check a copy
	return s.empty();
}

std::vector<std::string> content::strings::split(std::string_view s, char delim) {
	std::vector<std::string> result;
	std::istringstream content((std::string(s)));

	for (std::string token; std::getline(content, token, delim);) {
		// Insert the value
		result.push_back(std::move(token));
	}

	return result;
}
