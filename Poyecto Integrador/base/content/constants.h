#ifndef PROYECTOINTEGRADOR_CONSTANTS_H
#define PROYECTOINTEGRADOR_CONSTANTS_H

namespace content {
	/**
	 * Content separator item
	 */
	const char CONTENT_SEPARATOR = ';';

	/**
	 * Regular expression to check email files
	 */
	const char *EMAIL_REGEX_VALIDATOR = R"(^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$)";
}// namespace content


#endif//PROYECTOINTEGRADOR_CONSTANTS_H
