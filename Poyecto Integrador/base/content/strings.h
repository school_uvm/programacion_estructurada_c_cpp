#include <iostream>
#include <vector>

#ifndef PROYECTOINTEGRADOR_STRINGS_H
#define PROYECTOINTEGRADOR_STRINGS_H

namespace content::strings {

	/**
	 * Remove all blank spaces at the beginning of the string
	 *
	 * @param s The string to check
	 */
	void lTrim(std::string &s);

	/**
	 * Remove all blank spaces at the end of the string
	 *
	 * @param s The string to check
	 */
	void rTrim(std::string &s);

	/**
	 * Remove all blank spaces
	 *
	 * @param s The string to check
	 */
	void trim(std::string &s);

	/**
	 * Remove all blank spaces at the beginning of the string
	 *
	 * @param s The string to check
	 * @return The copy of result string
	 */
	std::string lTrimC(std::string s);

	/**
	 * Remove all blank spaces at the end of the string
	 *
	 * @param s The string to check
	 * @return The copy of result string
	 */
	std::string rTrimC(std::string s);

	/**
	 * Remove all blank spaces
	 *
	 * @param s The string to check
	 * @return The copy of result string
	 */
	std::string trimC(std::string s);

	/**
	 * Check if selected string is blank (This function ignores whitespace)
	 *
	 * @param s the content to check
	 */
	bool isBlank(std::string s);

	/**
	 * Split the string into multiple chunks depending on the delimiter
	 *
	 * @param s The string to check
	 * @param delim The character delimiter
	 * @return Returns a vector with all string chunks
	 */
	std::vector<std::string> split(std::string_view s, char delim = ' ');

}// namespace content::strings

#endif//PROYECTOINTEGRADOR_STRINGS_H
