#include <iostream>

#ifndef PROYECTOINTEGRADOR_FORMATSTR_H
#define PROYECTOINTEGRADOR_FORMATSTR_H

namespace content {

	class AskContent {
	public:

		virtual ~AskContent() = default;

		/**
		 * Ask for all files
		 *
		 * @param outStream Output stream
		 * @param inStream Input stream
		 */
		virtual void askForContent(std::ostream &outStream, std::istream &inStream) = 0;
	};

	class FormatString {
	public:

		/**
		 * Default destructor
		 */
		virtual ~FormatString() = default;

		/**
		 * Convert current instance object to formatted string
		 *
		 * @return Object string representation
		 */
		[[nodiscard]] virtual std::string toFormatString() const = 0;

		/**
		 * Pretty object string representation
		 *
		 * @return Pretty print representation
		 */
		[[nodiscard]] virtual std::string toString() const = 0;
	};

}// namespace content

#endif//PROYECTOINTEGRADOR_FORMATSTR_H
