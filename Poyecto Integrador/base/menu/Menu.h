#include "../../utils/utils.h"
#include "../raw_deps.h"

#ifndef PROYECTOINTEGRADOR_MENU_H
#define PROYECTOINTEGRADOR_MENU_H

namespace content::menu {

	/* -------------------------------------------------------------
	 * Declarations
	 * ------------------------------------------------------------- */

	/**
	 * Menu item class element declaration
	 */
	class MenuItem;

	/* -------------------------------------------------------------
	 * Sub types
	 * ------------------------------------------------------------- */

	/**
	 * Representation of collection index
	 */
	typedef long long Index;

	/**
	 * Representation of collection size
	 */
	typedef unsigned long long SizeC;

	/**
	 * Callback function definition
	 */
	typedef void (*Callback)(const MenuItem &);

	/**
	 * Empty callback definition
	 */
	typedef void (*OstreamCallback)(std::ostream &);

	/* -------------------------------------------------------------
	 * Menu item structure
	 * ------------------------------------------------------------- */

	/**
	 * Menu item definition
	 */
	class MenuItem {
	private:

		/**
		 * Item action
		 */
		Callback action = nullptr;

		/**
		 * Parent element
		 */
		MenuItem *parent = nullptr;

		/**
		 * Item name
		 */
		std::string name;

		/**
		 * Item description
		 */
		std::string description;

	public:

		/**
		 * Default constructor
		 *
		 * @param name Item name
		 * @param description Item description
		 * @param action Item action
		 */
		explicit MenuItem(std::string name, std::string description, Callback action = nullptr);

		/**
		 * Default constructor
		 *
		 * @param name Item name
		 * @param action Item action
		 */
		explicit MenuItem(std::string name, Callback action = nullptr);

		/**
		 * Default destructor
		 */
		virtual ~MenuItem() = default;

		/**
		 * Get the current item name
		 *
		 * @return  Returns the item name
		 */
		[[nodiscard]] std::string_view getName() const;

		/**
		 * Get the current item description
		 *
		 * @return  Returns the item description
		 */
		[[nodiscard]] std::string_view getDescription() const;

		/**
		 * Check if current item contains a parent element
		 *
		 * @return Returns @c true if current element contains a parent or @c false otherwise
		 */
		[[nodiscard]] bool hasParent() const;

		/**
		 * Get parent element
		 *
		 * @return Return the parent element instance
		 */
		[[nodiscard]] const MenuItem *getParent() const;

		/**
		 * Set item element
		 *
		 * @param item The item element pointer
		 */
		void setParent(const MenuItem *item);

		/**
		 * Set action callback to current item
		 *
		 * @param callback Target action callback. Can be a lambda expression
		 */
		void setAction(Callback callback);

		/**
		 * Display menu on the target stream
		 *
		 * @param stream The stream to display
		 * @param index Index element
		 * @param spaces Spaces between files
		 * @param max Max number of elements (used to calculate spaces)
		 */
		void display(std::ostream &stream, Index index, SizeC spaces = 1, SizeC max = 0) const;

		/**
		 * Execute item action
		 */
		void execute() const;
	};

	/* -------------------------------------------------------------
	 * Menu structure
	 * ------------------------------------------------------------- */

	class Menu : public MenuItem {

	private:

		/**
		 * Menu header files
		 */
		std::string header;

		/**
		 * Selection item separator
		 */
		std::string itemSeparator;

		/**
		 * Menu item container
		 */
		std::vector<std::shared_ptr<MenuItem>> itemContainer;

		/**
		 * Display parent info
		 *
		 * @param stream Target output stream
		 * @param spaces Total of spaces
		 */
		void displayParent(std::ostream &stream, SizeC spaces) const;

		/**
		 * Generate menu input ask
		 */
		void askForItem(std::ostream &stream) const;

		/**
		 * Default action if current menu is empty
		 */
		void defaultEmptyAction(std::ostream &stream) const;

		/**
		 * Define default menu action
		 *
		 * @param item The item selected
		 */
		static void defaultMenuAction(const MenuItem &item);

	public:

		/**
		 * Default constructor
		 *
		 * @param name Menu name
		 * @param description Menu description
		 * @param header Menu header files
		 */
		explicit Menu(std::string name, std::string description = "", std::string header = "");

		/**
		 * Default destructor
		 */
		~Menu() override;

		/**
		 * Check if current menu is empty and execute the action if is empty
		 *
		 * @return Returns @c true if menu is empty or @c false otherwise
		 */
		[[nodiscard]] bool isEmpty() const;

		/**
		 * Check if passed element exists
		 *
		 * @param item The item to search
		 * @param indexOf Index reference result
		 * @return Returns @c true if the element exists or @c false otherwise
		 */
		[[nodiscard]] bool exists(const std::shared_ptr<MenuItem> &item, Index *indexOf = nullptr) const;

		/**
		 * Get menu header files
		 *
		 * @return Returns the header info
		 */
		[[nodiscard]] std::string_view getHeader() const;

		/**
		 * Set menu files
		 *
		 * @param content The target files files
		 */
		void setHeader(std::string_view content);

		/**
		 * Get target item separator
		 *
		 * @return Returns the item separator
		 */
		[[nodiscard]] std::string_view getItemSeparator() const;

		/**
		 * Set menu item separator
		 *
		 * @param header The target item separator
		 */
		void setItemSeparator(std::string_view content);

		/**
		 * Get all registered items
		 *
		 * @return Returns all item in the current menu
		 */
		[[nodiscard]] std::list<std::shared_ptr<MenuItem>> getAllItems() const;

		/**
		 * Insert new item on the menu
		 *
		 * @param item The item to insert. This element can be a @c Menu instance
		 */
		void addItem(const std::shared_ptr<MenuItem> &item);

		/**
		 * Remove the element from the menu
		 *
		 * @param item The item to delete. This element can be a @c Menu instance
		 */
		void removeItem(const std::shared_ptr<MenuItem> &item);

		/**
		 * Display current menu on the target stream
		 *
		 * @param stream The target stream
		 */
		void showMenu(std::ostream &stream) const;

		/**
		 * Access to element via [] array access
		 *
		 * @param pos The element position
		 * @return Return the selected element or trow an error if not exists
		 */
		[[nodiscard]] const std::shared_ptr<MenuItem> &get(const Index &pos) const;
	};

}// namespace files::menu

#endif//PROYECTOINTEGRADOR_MENU_H
