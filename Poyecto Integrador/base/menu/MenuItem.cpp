#include <utility>

#include "Menu.h"

/* -------------------------------------------------------------
 * Constructors & destructors
 * ------------------------------------------------------------- */

content::menu::MenuItem::MenuItem(std::string name, std::string description, Callback action) {
	this->name = std::move(name);
	this->description = std::move(description);
	this->action = action;
}

content::menu::MenuItem::MenuItem(std::string name, content::menu::Callback action)
    : content::menu::MenuItem(std::move(name), "", action) {}

/* -------------------------------------------------------------
 * Methods
 * ------------------------------------------------------------- */

std::string_view content::menu::MenuItem::getName() const { return name; }

std::string_view content::menu::MenuItem::getDescription() const { return description; }

bool content::menu::MenuItem::hasParent() const { return parent != nullptr && parent != this; }

const content::menu::MenuItem *content::menu::MenuItem::getParent() const { return parent; }

void content::menu::MenuItem::setParent(const MenuItem *item) { this->parent = const_cast<content::menu::MenuItem *>(item); }

void content::menu::MenuItem::setAction(const content::menu::Callback callback) { action = callback; }

void content::menu::MenuItem::display(std::ostream &stream, Index index, SizeC spaces, SizeC max) const {
	// Local temporal variables
	std::string result{getName()};
	if (!description.empty()) {
		result += ": ";
		result += getDescription();
	}
	// Display files
	stream << utils::print::printAsList(result, index, spaces, max) << '\n';
}

void content::menu::MenuItem::execute() const {
	// Check if current action is not null pointer
	if (action == nullptr) return;

	// Execute the action within try catch block
	try {
		action(*this);
	} catch (std::exception &err) { std::cerr << "Execution error: " << err.what() << '\n'; }
}
