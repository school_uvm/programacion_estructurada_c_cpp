#include "Menu.h"
#include <algorithm>
#include <utility>

/* -------------------------------------------------------------
 * Constructors
 * ------------------------------------------------------------- */

content::menu::Menu::Menu(std::string name, std::string description, std::string header)
    : content::menu::MenuItem(std::move(name), std::move(description), nullptr) {
	// Initialize elements
	this->header = std::move(header);
	this->itemSeparator = "";
	// Set default elements
	this->setParent(this);
	this->setAction(defaultMenuAction);
}

content::menu::Menu::~Menu() {
	// Remove all elements from container
	itemContainer.clear();
}

/* -------------------------------------------------------------
 * Methods
 * ------------------------------------------------------------- */

bool content::menu::Menu::isEmpty() const { return itemContainer.empty(); }

bool content::menu::Menu::exists(const std::shared_ptr<MenuItem> &item, content::menu::Index *indexOf) const {
	// Always set indexOf as invalid
	if (indexOf != nullptr) *indexOf = -1;
	// Check if current menu is empty
	if (itemContainer.empty() || item == nullptr) return false;

	// Find element
	auto found = std::find(itemContainer.begin(), itemContainer.end(), item);
	bool foundStatus = found != itemContainer.end();

	if (foundStatus && indexOf != nullptr) *indexOf = found - itemContainer.begin();
	return foundStatus;
}

std::string_view content::menu::Menu::getHeader() const { return header; }

void content::menu::Menu::setHeader(std::string_view content) { header = content; }

std::string_view content::menu::Menu::getItemSeparator() const { return itemSeparator; }

void content::menu::Menu::setItemSeparator(std::string_view content) { itemSeparator = content; }

std::list<std::shared_ptr<content::menu::MenuItem>> content::menu::Menu::getAllItems() const {
	// Generate new list result
	auto result = std::list<std::shared_ptr<content::menu::MenuItem>>();
	// Pass by reference to prevent a pointer copy
	for (const auto &item : itemContainer) {
		// Only push on the list
		result.push_back(item);
	}

	return result;
}

void content::menu::Menu::addItem(const std::shared_ptr<MenuItem> &item) {
	// Check if the item exists
	if (exists(item)) return;
	// Insert item
	item->setParent(this);

	// Configure sub menus
	if (utils::casting::instanceOf<content::menu::Menu>(item.get())) {
		auto subMenu = std::dynamic_pointer_cast<content::menu::Menu>(item);
		std::string resultSeparator{"\t"};
		// Append the previews separator
		resultSeparator += getItemSeparator();
		subMenu->setItemSeparator(resultSeparator);
	}
	itemContainer.push_back(item);
}

void content::menu::Menu::removeItem(const std::shared_ptr<MenuItem> &item) {
	// Temporal variables
	content::menu::Index indexOf{};
	// Check if element exists
	if (exists(item, &indexOf)) return;

	// Remove the element (in this part "indexOf" will never be negative)
	itemContainer.erase(itemContainer.begin() + indexOf);// NOLINT(cppcoreguidelines-narrowing-conversions)
	item->setParent(nullptr);
}

void content::menu::Menu::showMenu(std::ostream &stream) const {
	// Check if current menu is empty
	if (isEmpty()) {
		defaultEmptyAction(stream);
		return;
	}
	// Display header information
	if (!header.empty()) stream << header << '\n';
	// Calculate the total spaces
	std::string rawSize = std::to_string(itemContainer.size());
	SizeC spaces = rawSize.size();

	// Check if the parent element exists
	if (hasParent()) displayParent(stream, spaces);

	// Iterate all elements
	for (int i = 0; i < itemContainer.size(); i++) {
		// Information vars
		int optionIndex{i + 1};
		auto item = itemContainer[i];
		// Display information
		if (!itemSeparator.empty()) stream << itemSeparator;
		item->display(stream, optionIndex, 1, spaces);
	}

	// Generate ask item
	askForItem(stream);
}

const std::shared_ptr<content::menu::MenuItem> &content::menu::Menu::get(const content::menu::Index &pos) const {
	//	std::cout << getName() << " Access to position: " << pos << '\n';
	if (pos < 0 || pos > itemContainer.size()) utils::errors::launch("Item access out of bounds.");
	return itemContainer[pos];
}

/* -------------------------------------------------------------
 * Internal methods
 * ------------------------------------------------------------- */

void content::menu::Menu::displayParent(std::ostream &stream, content::menu::SizeC spaces) const {
	// Display files information
	if (!itemSeparator.empty()) stream << itemSeparator;
	stream << utils::print::printAsList("Parent Menu...", 0, 1, spaces) << '\n';
}

void content::menu::Menu::askForItem(std::ostream &stream) const {
	// Temporal variables
	long long selected{};
	Index max = static_cast<Index>(itemContainer.size()) - 1;
	Index min = hasParent() ? -1 : 0;

	// Ask for any element
	utils::input::askForInput(&selected, "Select one option:", "Invalid input type");
	// Check if option is valid
	long long realSelected{selected - 1};
	if (realSelected < min || realSelected > max) {
		stream << "Index out of bounds" << '\n';
		showMenu(stream);
		return;
	}
	// Execute item
	if (realSelected == -1) {
		getParent()->execute();
	} else {
		auto selectedItem = itemContainer[realSelected];
		selectedItem->execute();
	}
	// Always execute another time show menu
	utils::input::wait(std::cin, stream);
	showMenu(stream);
}

void content::menu::Menu::defaultEmptyAction(std::ostream &stream) const {
	// Display info
	stream << "\"" << getName() << "\" menu is empty" << '\n';
	// Check if parent is empty
	if (getParent() == nullptr || !utils::casting::instanceOf<content::menu::Menu>(getParent())) return;
	// Store reference
	auto reference = dynamic_cast<const content::menu::Menu *>(getParent());
	if (reference == this) return;

	reference->showMenu(stream);
}

/* -------------------------------------------------------------
 * Static methods
 * ------------------------------------------------------------- */

void content::menu::Menu::defaultMenuAction(const content::menu::MenuItem &item) {
	// Check reference instance
	if (!utils::casting::instanceOf<content::menu::Menu>(&item)) return;
	// Cast element
	auto realItem = dynamic_cast<const content::menu::Menu *>(&item);
	realItem->showMenu(std::cout);
}
