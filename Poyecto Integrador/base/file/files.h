#include <fstream>
#include <iostream>
#include <string>

#ifndef PROYECTOINTEGRADOR_FILES_H
#define PROYECTOINTEGRADOR_FILES_H

namespace content::files {

	/**
	 * Database file extensions
	 */
	const std::string_view FILE_SUFFIX = "txt";

	/**
	 * Database for all students
	 */
	const std::string_view STUDENTS_FILE_CONTENT = "db_students";

	/* -------------------------------------------------------------
	 * Callbacks definitions
	 * ------------------------------------------------------------- */

	/**
	 * Line position
	 */
	typedef unsigned long long Line;

	/* -------------------------------------------------------------
	 * Functions
	 * ------------------------------------------------------------- */

	/**
	 * Require database file. If the file does not exist, it will try to create it.
	 *
	 * @param location The location to require
	 * @return Returns a valid file stream to manipulate
	 */
	std::fstream requireFile(std::string_view location);

	/**
	 * Truncate selected file
	 *
	 * @param location The file location to truncate
	 * @param stream The stream to truncate
	 */
	void truncateFile(std::string_view location, std::fstream &stream);

	/**
	 * Read selected stream and read line by line.
	 * This function not close the selected stream (be careful)
	 *
	 * @tparam T Generic type
	 * @param stream The stream to read
	 * @param action The action to process
	 */
	template<class T>
	void readLines(std::fstream &stream, T action) {
		// Check if the stream already closed or is not valid to read
		if (!stream.is_open()) throw std::runtime_error("The selected stream already closed.");

		// Reset stream position
		stream.clear();
		stream.seekg(0, std::ios::beg);

		// Temporal variables
		content::files::Line idx{1};
		std::string line;
		// Read the file to the end
		while (std::getline(stream, line)) {
			// Call the action
			action(idx++, line);
		}
	}

}// namespace content::files

#endif//PROYECTOINTEGRADOR_FILES_H
