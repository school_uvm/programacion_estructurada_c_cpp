#include "files.h"
#include "../../utils/errors.h"
#include <filesystem>
#include <sstream>// You don't need this if you use MinGW

std::string resolveFile(std::string_view location) {
	std::stringstream result;
	result << location << "." << content::files::FILE_SUFFIX;
	return result.str();
}

std::fstream content::files::requireFile(std::string_view location) {
	std::string realFile = resolveFile(location);
	std::fstream result;
	// Configure result
	result.exceptions(std::ifstream::badbit);

	// Open file
	try {
		if (std::filesystem::exists(realFile)) {
			result.open(realFile, std::fstream::in | std::fstream::out);
		} else {
			result.open(realFile, std::fstream::in | std::fstream::out | std::fstream::trunc);
		}
	} catch (std::ifstream::failure &e) { utils::errors::launch(e.what()); }

	// Get result
	return result;
}

void content::files::truncateFile(std::string_view location, std::fstream &stream) {
	std::filesystem::path realFile = resolveFile(location);
	// Truncate current file
	std::filesystem::resize_file(realFile, 0);
	// Reset stream position
	stream.clear();
	stream.seekg(0, std::ios::beg);
}
