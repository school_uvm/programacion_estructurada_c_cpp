#include <iostream>
#include "utils/input.h"
#include "utils/elements.h"
// Include exercises
#include "ejercicio_1/exercise1.h"
#include "ejercicio_2/exercise2.h"
#include "ejercicio_3/exercise3.h"
#include "ejercicio_4/exercise4.h"

/**
 * All exercises
 */
std::vector<utils::SelectionItem> exercises;

/**
 * Finish current execution
 */
void finish() {
	std::exit(0);
}

/**
 * Initialize all exercises if list is empty.
 *
 * If the elements do not exist then they are defined. This step is done here because @c c++
 * doesn't recommend static allocation, and that's because if there's an error off the stack,
 * it can't be handled and the application simply won't run.
 */
void initializeExercises() {
	// Check if list is empty
	if (!exercises.empty()) return;
	// Fill the list
	exercises = {
		utils::itemOf("Ejercicio 1", "Numero par e impar por medio de funciones", exercise1::execute),
		utils::itemOf("Ejercicio 2", "Calcula el promedio de varios valores", exercise2::execute),
		utils::itemOf("Ejercicio 3", "Suma de matices", exercise3::execute),
		utils::itemOf("Ejercicio 4", "Estructuras de usuario", exercise4::execute),
		utils::itemOf("Exit", "Close current program", finish)
	};
}

/**
 * Display menu on the screen
 */
void showMenu() {
	// Iterate all elements
	for (int i{0}; i < exercises.size(); i++) {
		// Information variables
		auto itemObj = exercises.begin()[i];
		int itemNum{i + 1};

		// Display information
		std::cout << "("
				  << itemNum
				  << ") "
				  << itemObj.name
				  << (itemObj.description.empty() ? "" : ": ")
				  << itemObj.description
				  << '\n';
	}
}

/**
 * Entry main point
 *
 * @return Return the program execution code
 */
int main() {
	// Initialize all elements
	initializeExercises();
	// Ask iteration and prevent any error
	try {
		while (true) {
			// Temporal information
			int option;

			// Show information and ask for option
			utils::input::printSectionShort("Menu", 0, 1);
			showMenu();
			utils::input::askInput(&option, "Select one exercise:", "Invalid number");

			// Check if option exists
			int realOption{option - 1};
			int max{static_cast<int>(exercises.size()) - 1};

			if (realOption < 0 || realOption > max) {
				std::cout << "Invalid option (" << option << "): Out of bounds" << '\n';
				continue;
			}

			// Get item and execute
			auto item = exercises.begin()[realOption];
			item.action();
		}
	} catch (std::exception &e) {
		std::cout << "Execution error: " << e.what() << '\n';
	}

	return 0;
}