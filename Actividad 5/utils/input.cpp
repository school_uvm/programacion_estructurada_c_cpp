#include "input.h"

void utils::input::printSection(std::string_view message, char item, char line, size_t size, size_t space_before,
								size_t space_after) {
	// Temporal generators
	std::string spacesB(space_before, '\n');
	std::string spacesA(space_after, '\n');
	std::string items(size, item);

	// Check spaces
	if (space_before != 0) std::cout << spacesB;
	// Display content
	std::cout << line << " " << items << '\n'
			  << line << " " << message << '\n'
			  << line << " " << items
			  << spacesA;
}

void utils::input::printSectionShort(std::string_view message, size_t space_before, size_t space_after) {
	printSection(message, '-', '*', 50, space_before, space_after);
}

void utils::input::clearInput() {
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void utils::input::wait() {
	std::cout << '\n' << "Press enter to continue...";
	std::cin.get();
	clearInput();
}

void utils::input::askInputStr(std::string *result, std::string_view message, std::string_view err, bool newLine) {
	// Store the input value
	std::string tmpValue;
	bool ok;

	// Iterate until the value is valid
	do {
		// Display question and ask for content
		std::cout << message << (newLine ? '\n' : ' ') << std::flush;

		// Check input type
		std::getline(std::cin >> std::ws, tmpValue, '\n');
		ok = std::cin.good();

		if (!ok) {
			utils::input::clearInput();
			std::cout << err << '\n' << std::flush;
			continue;
		}
		// Replace reference value
		*result = std::move(tmpValue);
	} while (!ok);

	// Discard content
	utils::input::clearInput();
}
