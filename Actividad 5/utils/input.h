#include <iostream>

#ifndef ACTIVIDAD_5_INPUT_H
#define ACTIVIDAD_5_INPUT_H

namespace utils::input {

	/**
	 * Generate console separator
	 *
	 * @param message Message to show
	 * @param item Line item
	 * @param line Line separator
	 * @param size Item total size
	 * @param space_before Lines inserted before section
	 * @param space_after Lines inserted after section
	 */
	void printSection(std::string_view message, char item = '-', char line = '*', size_t size = 50,
					  size_t space_before = 1, size_t space_after = 2);

	/**
	 * Generate console separator
	 *
	 * @param message Message to show
	 * @param space_before Lines inserted before section
	 * @param space_after Lines inserted after section
	 */
	void printSectionShort(std::string_view message, size_t space_before = 1, size_t space_after = 2);

	/**
	 * Remove input buffer
	 */
	void clearInput();

	/**
	 * Wait until user press enter
	 */
	void wait();

	/**
	 * Generates a question where the data type is verified and
	 * it will not stop executing until the value is correct or valid.
	 *
	 * @tparam TInput Generic result type
	 * @param result Result reference element
	 * @param message Question asked before data entry
	 * @param err Error message if value is invalid
	 * @param newLine If the value is true, a line break is inserted after the
	 * question, otherwise nothing is inserted.
	 */
	template<typename TInput>
	void askInput(
		TInput *result,
		std::string_view message,
		std::string_view err = "",
		bool newLine = false
	) {
		// Store the input value
		TInput tmpValue;
		bool ok;

		// Iterate until the value is valid
		do {
			// Display question and ask for content
			std::cout << message << (newLine ? '\n' : ' ') << std::flush;

			// Check input type
			std::cin >> tmpValue;
			ok = std::cin.good();

			if (!ok) {
				utils::input::clearInput();
				std::cout << err << '\n' << std::flush;
				continue;
			}

			// Replace reference value
			*result = std::move(tmpValue);
		} while (!ok);
	}

	/**
	 * Generates a question where the data type is verified and
	 * it will not stop executing until the value is correct or valid.
	 *
	 * @tparam TInput Generic result type
	 * @param result Result reference element
	 * @param message Question asked before data entry
	 * @param err Error message if value is invalid
	 * @param newLine If the value is true, a line break is inserted after the
	 * question, otherwise nothing is inserted.
	 */
	void askInputStr(
		std::string *result,
		std::string_view message,
		std::string_view err = "",
		bool newLine = false
	);

}

#endif //ACTIVIDAD_5_INPUT_H
