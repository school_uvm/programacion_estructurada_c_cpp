#include <iostream>
#include <functional>

#ifndef ACTIVIDAD_5_ELEMENTS_H
#define ACTIVIDAD_5_ELEMENTS_H

namespace utils {

    /**
     * Representation of function pointer
     */
    typedef std::function<void()> Callback;

    /**
     * Selection item type structure
     */
    struct SelectionItem {

        /**
         * Item name
         */
        std::string_view name;

        /**
         * Item description
         */
        std::string_view description;

        /**
         * Item action
         */
        Callback action;
    };

    /**
     * Generate a item easily
     *
     * @param name Item name
     * @param description Item description
     * @param action Item action to execute
     * @return
     */
    SelectionItem itemOf(std::string_view name, std::string_view description, const Callback &action);

}

#endif //ACTIVIDAD_5_ELEMENTS_H
