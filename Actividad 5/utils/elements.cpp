#include "elements.h"

utils::SelectionItem utils::itemOf(std::string_view name, std::string_view description, const utils::Callback &action) {
    return utils::SelectionItem{name, description, action};
}
