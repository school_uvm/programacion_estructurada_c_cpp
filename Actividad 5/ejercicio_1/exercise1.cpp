#include <iostream>
#include "exercise1.h"
#include "../utils/input.h"

bool exercise1::isPair(long long number) {
	return (number % 2) == 0;
}

void exercise1::execute() {
	// Show sections
	utils::input::printSection("Exercise 1");

	// Temporal variables
	long long selection;
	utils::input::askInput(&selection,
						   "Insert one number:",
						   "Invalid number");

	// Check if number is pair or odd
	std::cout << (exercise1::isPair(selection) ? "The number is pair" : "The number is odd") << '\n';
	utils::input::wait();
}
