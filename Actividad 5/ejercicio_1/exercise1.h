#ifndef ACTIVIDAD_5_EXERCISE1_H
#define ACTIVIDAD_5_EXERCISE1_H

namespace exercise1 {

	/**
	 * Determine if current number is pair or not
	 *
	 * @param number The number to analyze
	 * @return Returns @c true if number is pair or @c false if number is odd
	 */
	bool isPair(long long number);


	/**
	 * Exercise1 execution function
	 *
	 * @see utils::SelectionItem
	 */
	void execute();

}

#endif //ACTIVIDAD_5_EXERCISE1_H
