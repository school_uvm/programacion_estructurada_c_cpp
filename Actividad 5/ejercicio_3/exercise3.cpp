#include "exercise3.h"
#include "../utils/input.h"

float **exercise3::allocateMatrix(size_t cols, size_t rows) {
	// Allocate memory for columns
	auto **matrix = new float *[cols];
	for (int i = 0; i < cols; ++i) {
		// Allocate memory for rows
		matrix[i] = new float[rows];
	}
	return matrix;
}

void exercise3::deAllocateMatrix(float **matrix, size_t cols) {
	for (int i = 0; i < cols; ++i) {
		delete[] matrix[i];
	}
	delete[] matrix;
}

size_t exercise3::askSize(std::string_view message) {
	long long tmp;
	while (true) {
		utils::input::askInput(&tmp, message, "Invalid number");
		// Check if total is valid
		if (tmp >= 0) break;
		std::cout << "You must enter a number greater than \"0\"" << '\n';
	}
	// Replace result value
	return static_cast<size_t>(tmp);
}

void exercise3::printMatrix(float **matrix, size_t cols, size_t rows) {
	for (int c = 0; c < cols; c++) {
		for (int r = 0; r < rows; r++) {
			// Print each element
			std::cout << matrix[c][r];
			// Check if current index is the last element
			if (r < (rows - 1)) std::cout << ", ";
		}
		// Insert new line
		std::cout << '\n';
	}
}

void exercise3::sumOfArrays(float **matrix1, float **matrix2, float **result, size_t cols, size_t rows) {
	// Iterate all matrix elements
	for (int c = 0; c < cols; c++) {
		for (int r = 0; r < rows; r++) {
			result[c][r] = matrix1[c][r] + matrix2[c][r];
		}
	}
}

void exercise3::fillMatrix(int counter, float **matrix, size_t cols, size_t rows) {
	// Big message
	std::string counterS = std::to_string(counter);
	utils::input::printSectionShort("Matrix " + counterS);

	// Iterate all matrix elements
	for (int c = 0; c < cols; c++) {
		for (int r = 0; r < rows; r++) {
			// Make message
			char message[1024];
			std::sprintf(message, "Insert the value of array index [%d][%d]:", c, r);

			// Temporal input result
			float element;
			utils::input::askInput(&element, std::string_view(message), "Invalid number");

			// Set the matrix value
			matrix[c][r] = element;
		}
	}
}

void exercise3::execute() {
	// Store sizes
	size_t columns = askSize("How many columns:");
	size_t rows = askSize("How many rows:");

	// Check if matrix is empty
	if (columns == 0 || rows == 0) {
		std::cout << "Empty matrix" << '\n';
		utils::input::wait();
		return;
	}

	// Generate the matrix elements
	float **matrix1 = allocateMatrix(columns, rows);
	float **matrix2 = allocateMatrix(columns, rows);
	float **result = allocateMatrix(columns, rows);

	// Fill all arrays
	fillMatrix(1, matrix1, columns, rows);
	fillMatrix(2, matrix2, columns, rows);

	// Sum of arrays
	sumOfArrays(matrix1, matrix2, result, columns, rows);

	// Show the result
	std::cout << "Matrix one" << '\n';
	printMatrix(matrix1, columns, rows);

	std::cout << '\n' << "Matrix two" << '\n';
	printMatrix(matrix2, columns, rows);

	std::cout << '\n' << "Matrix result" << '\n';
	printMatrix(result, columns, rows);

	// Delete all arrays memory
	deAllocateMatrix(matrix1, columns);
	deAllocateMatrix(matrix2, columns);
	deAllocateMatrix(result, columns);
	utils::input::wait();
}
