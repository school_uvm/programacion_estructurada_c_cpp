#include <iostream>

#ifndef ACTIVIDAD_5_EXERCISE3_H
#define ACTIVIDAD_5_EXERCISE3_H

namespace exercise3 {

	/**
	 * Allocate memory on the matrix
	 *
	 * @param cols Total of columns
	 * @param rows Total of rows
	 */
	float **allocateMatrix(size_t cols, size_t rows);

	/**
	 * Deallocate memory from the matrix
	 *
	 * @param matrix The matrix to allocate
	 * @param cols Total of columns
	 */
	void deAllocateMatrix(float **matrix, size_t cols);

	/**
	 * Ask for any size
	 *
	 * @param message The message to ask
	 * @return Return selected size
	 */
	size_t askSize(std::string_view message);

	/**
	 * Display a multi-dimensional matrix on the console
	 *
	 * @param matrix The multi-dimensional matrix to display
	 * @param cols Total of columns
	 * @param rows Total of rows
	 */
	void printMatrix(float **matrix, size_t cols, size_t rows);

	/**
	 * Perform a matrix sum
	 *
	 * @param matrix1 The first matrix
	 * @param matrix2 The second matrix
	 * @param result The matrix result
	 * @param cols Total of columns
	 * @param rows Total of rows
	 */
	void sumOfArrays(float **matrix1, float **matrix2, float **result, size_t cols, size_t rows);

	/**
	 * Fill all matrix content
	 *
	 * @param counter Current matrix index
	 * @param matrix The multi-dimensional matrix to fill
	 * @param cols Total of columns
	 * @param rows Total of rows
	 */
	void fillMatrix(int counter, float **matrix, size_t cols, size_t rows);

	/**
	 * Exercise 3 function execution
	 */
	void execute();

}

#endif //ACTIVIDAD_5_EXERCISE3_H
