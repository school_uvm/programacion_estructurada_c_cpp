#include <iostream>

#ifndef ACTIVIDAD_5_EXERCISE4_H
#define ACTIVIDAD_5_EXERCISE4_H

namespace exercise4 {

	/**
	 * Student structure content
	 */
	struct Student {
		/**
		 * Student card id
		 */
		std::string id;

		/**
		 * Student name
		 */
		std::string name;

		/**
		 * Student career
		 */
		std::string career;

		/**
		 * Student average
		 */
		float average;

		/**
		 * Student full address
		 */
		std::string address;

	};

	/**
	 * Fill student information
	 *
	 * @param student The student to fill
	 */
	void fillStudent(Student *student);

	/**
	 * Print the student info on the screen
	 *
	 * @param student The student to show
	 */
	void printStudent(Student &student);

	/**
	 * Exercise function execution
	 */
	void execute();


}

#endif // ACTIVIDAD_5_EXERCISE4_H