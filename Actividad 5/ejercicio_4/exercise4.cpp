#include "exercise4.h"
#include "../utils/input.h"

void exercise4::fillStudent(exercise4::Student *student) {
	// Ask for all elements
	utils::input::askInputStr(&student->id, "Insert your id:");
	utils::input::askInputStr(&student->name, "Insert your name:");
	utils::input::askInputStr(&student->career, "Insert your career:");
	utils::input::askInput(&student->average, "Insert your average:");
	utils::input::askInputStr(&student->address, "Insert your address:");
}

void exercise4::printStudent(exercise4::Student &student) {
	std::cout << "Student id: " << student.id << '\n';
	std::cout << "Student name: " << student.name << '\n';
	std::cout << "Student career: " << student.career << '\n';
	std::cout << "Student average: " << student.average << '\n';
	std::cout << "Student address: " << student.address << '\n';
}

void exercise4::execute() {
	// Temporal variables
	Student result;

	// Ask for student
	fillStudent(&result);

	// Print the student
	printStudent(result);

	utils::input::wait();
}