#include "exercise2.h"
#include "../utils/input.h"

float exercise2::calculateScore(const float scores[], size_t length) {
	// Store the sum
	float sum{};

	// Iterate all elements
	for (int i = 0; i < length; i++) {
		sum += scores[i];
	}

	// Convert length to float to prevent implicit conversion
	// to size_t and lose some information
	return sum / static_cast<float>(length);
}

size_t exercise2::askForTotal() {
	long long tmp;
	while (true) {
		utils::input::askInput(&tmp,
							   "Insert the total scores: ",
							   "Invalid number");
		// Check if total is valid
		if (tmp >= 1) break;
		std::cout << "You must enter a number greater than \"0\"" << '\n';
	}
	// Replace result value
	return static_cast<size_t>(tmp);
}

void exercise2::fillScores(float scores[], size_t length) {
	// Iterate all scores
	for (int i = 0; i < length; i++) {
		// Temporal variables
		float element;
		std::string index = std::to_string(i + 1);
		std::string quest{"(" + index + ")" + " Insert your score:"};

		// Ask for elements
		while (true) {
			utils::input::askInput(&element, quest, "Invalid number");

			// Check if  number is not lower than 0
			if (element >= 0) break;
			std::cout << "You must enter a number greater than or equal to \"0\"" << '\n';
		}

		// Insert the element into the array
		scores[i] = element;
	}
}

void exercise2::execute() {
	// Variables
	size_t total = askForTotal();
	auto *buffer = new float[total];

	// Fill all content
	fillScores(buffer, total);

	// Calculate the final score and show on the screen
	float finalScore = calculateScore(buffer, total);
	std::cout << "Your final score is: " << finalScore << '\n';

	// Delete all dynamic memory
	delete[] buffer;
	utils::input::wait();
}
