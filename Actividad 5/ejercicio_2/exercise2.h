#include <iostream>

#ifndef ACTIVIDAD_5_EXERCISE2_H
#define ACTIVIDAD_5_EXERCISE2_H

namespace exercise2 {

	/**
	 * Calculate the score of all elements
	 *
	 * @param scores All scores to calculate
	 * @param length Total elements
	 * @return Return the score of all elements
	 */
	float calculateScore(const float scores[], size_t length);

	/**
	 * Ask to user for total scores
	 *
	 * @return Returns the total score container
	 */
	size_t askForTotal();

	/**
	 * Fill all array scores
	 *
	 * @param scores The scores to fill
	 * @param length Total scores
	 */
	void fillScores(float scores[], size_t length);

	/**
	 * Exercise 2 function execution
	 */
	void execute();

}


#endif //ACTIVIDAD_5_EXERCISE2_H
