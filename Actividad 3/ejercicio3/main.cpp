#include <iostream>
#include "../utilities/my_io.h"

int main() {
    // Temporal variables
    long long inserted{};
    // Ask for number
    extra::io::cInput(&inserted, "Insert one number:", "Invalid number.");

    // Evaluate if number is pair or not
    if (inserted % 2 == 0) {
        std::cout << "The number \"" << inserted << "\" " << "is pair" << '\n';
    } else {
        std::cout << "The number \"" << inserted << "\" " << "is not pair" << '\n';
    }
}