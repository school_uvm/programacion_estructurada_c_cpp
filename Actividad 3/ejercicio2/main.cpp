#include <iostream>
#include "../utilities/my_io.h"

// Min value
const int C_MIN{0};
// Max value
const int C_MAX{10};


int main() {
    short score;
    do {
        extra::io::cInput(&score, "Inserta tu calificacion:", "Numero invalido");

        // Check if number is valid
        if (score < C_MIN || score > C_MAX) {
            std::cout << "Error" << '\n';
            continue;
        }

        // Inspect the value
        switch (score) {
            case 10:
                std::cout << "Acreditado" << '\n';
                break;
            case 9:
                std::cout << "Muy bien" << '\n';
                break;
            case 8:
                std::cout << "Bien" << '\n';
                break;
            case 7:
                std::cout << "Regular" << '\n';
                break;
            default:
                std::cout << "No acreditado" << '\n';
                break;
        }
        // Break execution
        break;
    } while (true);

    return 0;
}