#include <iostream>
#include "../utilities/my_constants.h"

int main() {
    // Only if you want a very long numbers
    long long selected;

    // Ask for number
    std::cout << "Insert one number: ";
    std::cin >> selected;
    // Check if inserted value is valid
    if (!std::cin.good()) {
        std::cout << "Invalid number. Good bye :D!" << extra::C_JUMP;
        return -1;
    }

    // Process valid number
    if (selected % 2 == 0) {
        std::cout << "The number \"" << selected << "\" is pair.";
    } else {
        std::cout << "The number \"" << selected << "\" is odd.";
    }

    return 0;
}