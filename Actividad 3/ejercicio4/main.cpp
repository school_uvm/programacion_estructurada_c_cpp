#include <iostream>
#include <string>
#include "../utilities/my_io.h"

int main() {
    int totalArticles;
    float totalPrice{0};
    extra::io::cInput(&totalArticles, "How many articles?", "Invalid number");

    // Iterate all articles
    for (int i = 0; i < totalArticles; i++) {
        int units;
        float unitPrice;
        // Temporal variables
        auto iStr = std::to_string(i + 1);
        auto questQ = "Article " + iStr + ". How many articles?";
        auto questP = "Article " + iStr + ". How much does the item cost (per unit)?";

        // Ask 1
        extra::io::cInput(&units, questQ, "Invalid number");
        // Ask 2
        extra::io::cInput(&unitPrice, questP, "Invalid number");

        totalPrice += (unitPrice * static_cast<float>(units));
    }

    // Show total price
    std::cout << "The result of the cost of all items is: $" << totalPrice << extra::C_JUMP << std::flush;
    return 0;
}