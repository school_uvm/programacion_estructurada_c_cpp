# Actividad 3

Conceptos basicos de la programación. Donde se aprende a usar las estructuras de datos, ciclos y variables.

- [Ejercicio 1](./ejercicio1/main.cpp)
- [Ejercicio 2](./ejercicio2/main.cpp)
- [Ejercicio 3](./ejercicio3/main.cpp)
- [Ejercicio 4](./ejercicio4/main.cpp)