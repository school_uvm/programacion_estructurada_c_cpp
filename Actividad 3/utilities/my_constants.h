#ifndef UTILITIES_CONSTANTS_H
#define UTILITIES_CONSTANTS_H

namespace extra {

    /**
     * @brief Tabulation character
     */
    const char C_TAB = '\t';

    /**
     * @brief New line character
     */
    const char C_JUMP = '\n';
}

#endif // UTILITIES_CONSTANTS_H