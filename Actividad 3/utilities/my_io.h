#ifndef UTILITIES_IO_H
#define UTILITIES_IO_H

#include <iostream>
#include "my_constants.h"

namespace extra::io
{

    /**
     * @brief Clear input buffer and clear all stored buffer to prevent
     * infinite loops.
     */
    void clearStdInput()
    {
        std::cin.clear();
        std::cin.ignore(
            std::numeric_limits<std::streamsize>::max(),
            extra::C_JUMP);
    }

    /**
     * @brief Generates a question where the data type is verified and
     * it will not stop executing until the value is correct or valid.
     *
     * @tparam T Generic result type
     * @param result Result reference element
     * @param message Question asked before data entry
     * @param err Error message if value is invalid
     * @param jump_line If the value is true, a line break is inserted after the
     * question, otherwise nothing is inserted.
     */
    template <typename T>
    void cInput(
        T *result,
        const std::string &message,
        const std::string &err,
        bool jump_line = false)
    {
        static_assert(std::is_fundamental<T>::value, "Invalid primitive type");
        bool ok;
        // Iterate until the value is valid
        do
        {
            T selected;

            // Ask for number
            // Flush used to prevent strange format behavior
            std::cout << message << (jump_line ? extra::C_JUMP : ' ') << std::flush;

            std::cin >> selected;
            ok = std::cin.good();

            if (!ok)
            {
                extra::io::clearStdInput();
                std::cout << err << extra::C_JUMP << std::flush;
                continue;
            }

            // Replace reference value
            *result = selected;
        } while (!ok);
    }

}
#endif // UTILITIES_IO_H