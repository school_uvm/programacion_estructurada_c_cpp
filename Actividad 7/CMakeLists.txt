cmake_minimum_required(VERSION 3.20)

project(Actividad_7_C C)
set(CMAKE_C_STANDARD 11)
add_executable(Actividad_7_C
        c/main.c)

project(Actividad_7_CPP)
set(CMAKE_CXX_STANDARD 17)
add_executable(Actividad_7_CPP
        cpp/main.cpp)