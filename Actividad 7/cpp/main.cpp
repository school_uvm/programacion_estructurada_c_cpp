#include <iostream>
#include <string>
#include <locale>

// Convertimos el texto a minusculas
std::string str_tolower(const std::string &str, const std::locale &locale) {
	// Creamos una nueva cadena
	std::string result{};
	// Iteramos todas las letras
	for (auto item: str) {
		result += std::tolower(item, locale);
	}

	return result;
}

// Convertimos el texto a mayusculas
std::string str_toupper(const std::string &str, const std::locale &locale) {
	// Creamos una nueva cadena
	std::string result{};
	// Iteramos todas las letras
	for (auto item: str) {
		result += std::toupper(item, locale);
	}

	return result;
}

int main() {
	std::locale locale; // Tomamos el valor por defecto
	std::string text;
	std::string result;
	char format{};

	// Le pedimos al usuario que ingrese una cadena de texto
	std::cout << "Ingrese una cadena de texto: ";
	std::getline(std::cin >> std::ws, text);

	// Preguntamos sobre el formato
	do {
		std::cout << "0. Minusculas" << '\n';
		std::cout << "1. Mayusculas" << '\n';
		std::cout << "Selecciona un formato: ";
		std::cin >> format;
		// Limpiamos el buffer antes de volver a preguntar
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	} while (format != '0' && format != '1');

	result = text;
	// Verificamos la opción seleccionada
	if (format == '0')
		result = str_tolower(text, locale);
	if (format == '1')
		result = str_toupper(text, locale);

	// Mostramos el resultado
	std::cout << "Original: " << text << '\n';
	std::cout << "Resultado: " << result << '\n';

	return 0;
}