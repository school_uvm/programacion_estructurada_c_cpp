#include <stdio.h>
#include <string.h>
#include <ctype.h>

// Definimos el maximo de caracteres de entrada
#define INPUT_MAX 1024
// Realizamos esta operación porque algunos compiladores
// pueden usar los tipos char diferente
#define INPUT_MAX_SIZE INPUT_MAX * sizeof(char)

// Convertimos el texto a minusculas
void str_tolower(char *content) {
	for (int i = 0; content[i] != '\n'; ++i) {
		content[i] = (char) tolower(content[i]);
	}
}

// Convertimos el texto a mayusculas
void str_toupper(char *content) {
	for (int i = 0; content[i] != '\n'; ++i) {
		content[i] = (char) toupper(content[i]);
	}
}

int main() {
	// Generamos variables para los resultados, memoria suficiente para
	// escribir textos largos (para este ejemplo)
	char content[INPUT_MAX_SIZE];
	char format;

	// Mostramos mensaje para que el usuario inserte una cadena
	// de caracteres
	printf("Inserte una cadena de caracteres: ");

	// Realizamos la recolección de datos
	gets(content);

	// Le damos a elegir el formato de salida
	do {
		printf("0. Minusculas\n");
		printf("1. Mayusculas\n");
		printf("Selecciona un formato: ");
		scanf("%c", &format);
	} while (format != '1' && format != '0');

	// Convertimos el texto
	char contentResult[INPUT_MAX_SIZE];
	strcpy(contentResult, content);

	for (int i = 0; content[i] != '\0'; ++i) {
		if (format == '0') str_tolower(contentResult);
		if (format == '1') str_toupper(contentResult);
	}

	// Mostramos el resultado
	printf("Original: %s\n", content);
	printf("Resultado: %s\n", contentResult);
	return 0;
}
